+++
banner     = "banners/2017-10-25-blondinka-za-rulem-nachalo.jpeg"
title      = "Блондинка за рулем: начало"
author     = "Lana"
date       = "2017-10-25"
categories = ["car"]
tags       = ["fordfocus"]
+++

Проснувшись утром и увидев на окне иней и замерзшие капли дождя меня охватила паника. Мало того, что резина летняя, так она еще и изрядно лысая. Как в школу ехать по гололеду? Я же еще не эксперт в экстремальном вождении.

<!--more-->

## Визит на шиномонтаж

При более детальном рассмотрении оказалось, что на улице идет дождь, а температура за бортом ниже нуля не опустилась: +4. Самое время ехать переобуваться.

Зимняя резина хранилась у папы в гараже в 24 км от моего дома. Делать нечего, ребенка в школу завезла и поехала за резиной. Закинули шины в папину машину, решили съездить на шиномонтаж в гаражах, посмотреть, что там делается. Думали, если будет очередь на полдня, папа сам переобует мою машину. Приехали на шиномонтаж, а там пусто, работники курят стоят. И такие, мол, давайте быстрее, пока никого нет. Пока папа выгружал шины я побежала за машиной своей. Бегу я, значит, а тут муж звонит. Как всегда вовремя, конечно. Говорю: не могу разговаривать, я бегу. После некоторого молчания муж уточняет: ты убегаешь от кого-то? что уже натворила?

Мастера колеса меняют, а я кругами вокруг них хожу и заглядываю под руку. Сильно мне было интересно, что они там делают и как процесс этот происходит. Ни с первой, ни со второй машиной я технической стороной никогда не интересовалась. Я только ездила. А все эти СТО, переобувания и расходники &#8211; этим муж заведовал. А теперь получилось так, что у меня больше свободного времени, поэтому все вопросы по машине решаю я. Пришлось, так сказать.

Процесс переобувания идет, а мой папа, хозяйственник крепкий, шины летние в машину себе грузит и приговаривает: &#8220;Да на них еще половину сезона откатать можно, не будем пока выбрасывать, пусть полежат&#8221;. А мастер услышал и мне тихонько так говорит: &#8220;Меняйте обязательно, шинам уже лет и лет, да и лысоватая она&#8221;.

20 минут заняла эта процедура смены резины и еще час добраться домой. Но на обратном пути я уже раcслабилась, никуда торопиться не надо, ребенка со школы забрать успеваю, да и резина мне внушает теперь спокойствие и уверенность.

## История из жизни

Вот еще вспомнилось из серии &#8220;блондинка за рулем&#8221;, хоть я и не блондинка давно: на первый или второй день после покупки машины договорились мы встретиться со страховым агентом. Она должна была мне полис привезти и машину пофографировать. Чтобы был пруф, что машина на момент страхования повреждений не имела. Ну и помимо всего прочего говорит мне открыть капот, чтобы внутренности сфотографировать. Так, думаю, собралась и быстренько вспомнила, как капот открывать. На шниве был рычаг в салоне, а тут ключом открывать надо. Собралась, открыла и держу его. Говорю агенту: &#8220;Фотографируйте, пока я держу. Или можем вон того мужчину попросить подержать&#8221;. Глядя, как у нее брови удивленно поползли вверх я внезапно вспомнила, как крышка капота подпирается. И так мне стало с самой себя смешно, вот истинная блондинка за рулем, не иначе.

Пару дней назад, кстати, вспомнила, что надо задние колодки поменять. Бывший владелец перед продажей передние заменил, а задних на Виннере не было в наличии. Нашла автомагазин, где есть колодки Брембо (у меня передние такие), поговорила дополнительно с менеджером магазина, чтобы удостовериться в верности своего выбора. Вот жду, пока колодки привезут и можно ехать на сто менять их. Параллельно про сайлентблок читаю. Их тоже надо заменить. Но это уже будет уже другая история из жизни обезьяны за рулем.

Кстати, [здесь][1] я писала про покупку и установку автокресла, а вот [тут][2]

про перерегистрацию автомобиля в Украине.

 [1]: ../2017-10-11-avtokreslo-rebenku-7-let/
 [2]: ../2017-10-07-peregistratsiya-avtomobilya-v-ukraine/
