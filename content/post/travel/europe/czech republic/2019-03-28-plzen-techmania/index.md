+++
banner     = "banners/2019-03-28-plzen-techmania.jpg"
title      = "Научный центр Техмания в Пльзене"
author     = "Lana"
date       = "2019-03-28"
categories = ["travel"]
tags       = ["europe", "czechrepublic"]
+++
---
Второй день в городе  Пльзень мы провели в Научном центре "Техмания". [Первый день](https://lanadays.com/post/prague/2019-03-25-plzen-zoo/) провели в Зоопарке и гуляли по городу.

После завтрака в отеле мы поехали в "Техманию". Мы как раз приехали к 10 утра к открытию. Припарковали наш гордый фордик на улице.

Купили билеты, которые, кстати действуют также на посещение планетария. Забегая наперед скажу, что на планетарий у нас,к сожалению, не хватило времени и сил. Но я надеюсь, мы не последний раз в этом городе.

<!--more-->

![](img/ford.jpg)

![](img/happy.jpg)

Помещение центра просто огромное. Сама идея такого центра - познакомить детей с наукой, показать природные явления, из чего состоит и как функционирует человеческое тело, дать возможность самим все пощупать и проверить.

У нас в Киеве тоже есть подобное заведение "Экспериментаниум". Но там как-то тесно, места маловато. А тут раздолье, людей много, но очередей к экспонатам нету.

![](img/techmania.jpg)

Само помещение визуально разделено на несколько частей: вода, оптика, электричество, питание и все такое. Все хочется потрогать и попробовать. Текста будет мало, фото много. Муж по ходу дела снимал видео, оно более информативно. В конце дам ссылку.

Верхнюю одежду можно сдать в гардероб или сложить вещи в ячейку под ключик. Тут же есть кафе с комплексными обедами и напитками. Вкусно и не дорого. Столиков много, места всем хватает. Мне нравится когда не нужно с подносом стоять и ждать, пока освободится столик.

Итак, как только зашли  - начали с завязывания морского узла.

![](img/sea.jpg)

И пошли все пробовать.

Что понравилось ребенку:

* попадать в кольцо надувным мячом, регулируя силу подачи воздуха;
![](img/ball.jpg)

* зажигать лампочки (причем как прямой подачей напряжения, так и через генератор, очень наглядно);
![](img/lamps_1.jpg)
![](img/lamps_2.jpg)

* затихающие колебания;
![](img/waves.jpg)

* потопить корабль с помощью пузырьков (я ее начинаю опасаться, Настюшка-опасность =);
![](img/ship.jpg)

Не могли вытащить из водного отдела. Там можно и кораблики попускать, и дамбу постороить, и водяное колесо покрутить и даже просеять песок в поисках золота.

![](img/water_1.jpg)

![](img/water_2.jpg)

Также здесь проводят всякие опыты. По громкоговорителю объявляют о начале программы и зрители стекаются к сцене. Тут детки могут поучаствовать в программе, побыть ассистентом астронавта на марсе или почувствовать на себе статическое электричество.

![](img/show_2.jpg)

Понятное дело программа на чешском языке.

После опытов решили пойти пообедать. После еды и кофе мы снова были готовы все пробовать.

Дошли до симпатичного отдела: человек и зверь.

![](img/clovek.jpg)

Тут можно узнать скорость бега, длину прыжка и сравнить свои результаты с животными, а также послушать звуки разных зверей.

Вот что меня поразило больше всего, так это наклонный дом. При сравнительно небольшом угле наклона мозг с трудом воспринимает это несоответствие и начинает кружиться голова. Второй и третий раз дались проще. Муж говорит, если смотреть через экран камеры так вообще все хорошо.

![](img/house_tech.jpg)

А еще классная эмуляция тонущего корабля. Часть корабля под наклоном, как будто половина уже под воду ушла, работает сигнализация и мигает освещение. Ух, полное погружение прямо.

И напоследок я узнала как работает унитаз.

![](img/toliet.jpg)

Как и обещала вот наше [видео](https://www.youtube.com/watch?v=4Otk14c9vXg&t=97s) из "Техмании".

В целом, очень классный центр. Интересно не только детям, но и взрослым. Планируйте целый день на посещение этого центра. А еще не забудьте посетить Планетарий.
