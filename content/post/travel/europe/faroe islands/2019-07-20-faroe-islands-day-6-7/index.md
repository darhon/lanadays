+++
banner     = "banners/2019-07-20-faroe-islands-day-6-7.jpg"
title      = "Фарерские острова: дни 6-7"
author     = "Lana"
date       = "2019-07-20"
categories = ["travel"]
tags       = ["europe", "faroe", "denmark"]

+++

Утром неспеша позавтракали, собрали свой нехитрый скарб и поехали в столицу погулять. С утра светило солнце и погода в целом очень располагала к прогулке.

<!--more-->
## 6 день

В городе Tórshavn (столице) очень красивая набережная.

![](img/pic_11.jpg)

![](img/pic_12.jpg)

![](img/pic_13.jpg)

Много лодок, яхт и обычных рыбацких лодок. Тут же располагается офис мера города и его дом.

![](img/pic_14.jpg)

Есть небольшой порт, мы наблюдали как причаливал большой теплоход.

![](img/pic_15.jpg)

![](img/pic_16.jpg)

Узкие улочки, красивые деревянные дома. На каждом доме фамилия проживающей семьи. Приятно здесь погулять.

![](img/pic_17.jpg)

![](img/pic_18.jpg)

Позавтракать зашли в Essabarr - кафе недалеко от набережной с открытой терассой. Несколько вариантов завтраков, неплохой кофе и возможность посидеть на солнышке любуясь набережной - что еще нужно голодному туристу?

![](img/pic_20.jpg)

После еды поехали на соседний остров Eysturoy заселяться в Gjaargardur Guesthouse Gjogv (в городе Gjogv). Это около часа пути (70 км).
Номера в этом гестхаузе расположены в екс-жилых домах.
Маленький, но уютный номер.

![](img/pic_21.jpg)

![](img/pic_22.jpg)

![](img/pic_23.jpg)

Вид из дверей номера:

![](img/pic_2.jpg)

Много разношерстных туристов: французы, датчане, немцы и японцы. Встретили также интересных поляков, один мужчина рассказывал, как он два раза был на Аляске.

В этом городке располагается туристическая тропа на холм. С него открывается замечательный вид на соседний остров и на сам городок.

![](img/pic_25.jpg)

![](img/pic_26.jpg)

![](img/pic_27.jpg)

![](img/pic_28.jpg)

![](img/pic_29.jpg)

![](img/pic_30.jpg)

На вершине холма ребенок прилег порисовать.

![](img/pic_31.jpg)

Очень красиво:

![](img/pic_32.jpg)

![](img/pic_33.jpg)

Ну и без беганья за овечками не обошлось:

![](img/pic_34.jpg)

![](img/pic_35.jpg)

![](img/pic_36.jpg)

Ужинали в ресторанчике при отеле. Шведский стол, много рыбы во всех видах: жареная, тушеная, в салатах и свежесоленая.

Я взяла попробовать интересное мясо, по виду похожее на вяленое. Небольшие такие кусочки говядины. Сев за столик я сначала не поняла, что воняет. В общем, это оказалось ферментированное мясо. Оно воняет так, как будто сдохло давным давно и только благодаря магии не потеряло свой вид. Я, конечно, уговорила себя это съесть. Когда еще будет такая возможность попробовать что-то непривычное. Мужу тоже подсунула кусочек. Не одной же мне страдать. Вкуса не ощущаешь, потому что хочется это быстрее проглотить. Не знаю, как и зачем люди это едят, но я больше такое пробовать не буду.

После ужина отправились спать.

## 7 день

Выходишь утром за порог, а тебя встречает обалденный вид:

![](img/pic_37.jpg)

![](img/pic_38.jpg)

На завтрак опять таки шведский стол: сухие завтраки, сыр, нарезка, булочки и яйца.

После завтрака поехали подниматься на гору. Часа 1,5 занял у нас подъем. Местами карабкались по камням. Но в целом дорожка была достаточно комфортной для взбирания в гору.

![](img/pic_39.jpg)

![](img/pic_40.jpg)

![](img/pic_42.jpg)

Ну и ради чего мы сюда лезли - это виды. Просто потрясающие.

![](img/pic_41.jpg)

![](img/pic_43.jpg)

Спустились с горы и поехали в город Eiði пообедать.

Вот этот красивый городок:

![](img/pic_45.jpg)

Кстати, почти в каждой более-менее большой деревне есть футбольное поле. В этой местности футбол уважают и увлеченно им занимаются.

Ну и конечно же кроме кофейни в жилом доме, где нам предложили кофе и тортик, мы ничего не нашли. А обедать сладким не захотели.

При въезде в город приметили место отдыха: парковка, туалеты и столики.

![](img/pic_44.jpg)

Поэтому зашли в маркет и купили еды: бутерброды, салат, творог и кофе. А еще купили непонятное зеленое пойло, которое пить невозможно было. При детальном рассмотрении оказалось, что это концентрат, который нужно разводить в пропорции: литр пойла на 11 литров воды. Ну в ведре разве что можно намешать себе пойла на неделю. Мы его привезли с собой в Прагу, недели две оно постояло на кухне, к нему никто не притронулся и я его вылила.

После еды поехали дальше. Полюбовались ветряными электростанциями. Они оказываются почти бесшумные. Мне казалось они должны гудеть, а по факту тихенькие совсем.

![](img/pic_48.jpg)

На обратном пути еще в одну деревеньку заехали. Буквально на пару домов.

![](img/pic_46.jpg)

![](img/pic_47.jpg)

Вчерашний ужин в отеле нам сильно дорого обошелся. Поэтому решили в этот день съекономить и набрали вкусностей на ужин в маркете: сыра, рыбы, йогуртов и хлеба.
После ужина собрали вещи и засели каждый в своем девайсе. Утром нам оставалось только выписаться из гестхауза и по пути в аэропорт заправить машину.

Прощайте, Фарерские острова. Это незабываемая красота, природа и минимум туристов. Скоро это место будет популярным и хорошо, что мы успели попасть сюда.

![](img/pic_49.JPG)
