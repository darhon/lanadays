+++
banner     = "banners/2019-09-13-ostrov-krk-i-rovinj.jpg"
title      = "Автомобилем по Хорватии: остров Крк (Пунат) и Ровинь"
author     = "Lana"
date       = "2019-09-13"
categories = ["travel"]
tags       = ["europe", "croatia", "fordfocus"]
+++
---

В [предыдущем](https://lanadays.com/post/travel/europe/croatia/2019-09-09-zagreb/) посте я писала про Загреб. После посещения столицы наш путь лежал на море на остров Крк в город Пунат.

<!--more-->

<h2>Пунат</h2>

От Загреба по Пуната 189 км, но по времени занимает немало: 2,5 часа езды. Потому что по острову ехать быстро не получается - много крутых поворотов, а также большое количество машин. Средняя скорость по нему получается около 40-50 км/час.

После завтрака магазинными йогуртами и булочками мы двинули в путь к морю. Я очень люблю море. Мне нравится этот запах, шум волн, крики чаек и конечно же плавать.

Остров Крк местные жители называют "солнечный остров". Здесь много оливковых плантаций, виноградников и хвойных деревьев. А вода удивительного цвета.

<a href="img/IMG_6793.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6793.jpg"/>
</a>

Я хотела посмотреть город Башка и столицу острова - одноименный город Крк. Но на острове у нас был всего один полный день и мы решили, что хотим провести его по шею в море.
В городе Пунат мы арендовали апартаменты на первом этаже с небольшим двориком, где стоял стол, сушилка для белья и гриль. После тесных и душных комнат в Загребе нам эти хоромы показались просто райским уголком. Обошлось нам это жилье недешево, а все потому, что мы бронировали его за неделю до приезда. Почему так получилось? Расскажу.

За пару месяцев до отпуска мы продумали маршрут, забронировали во всех городах жилье и даже загнали машину на СТО проверить жидкости. В общем, подготовились по полной программе. И тут за неделю до выезда мы случайно увидели, что жилье в Хорватии у нас забронировано на 2020 год. Те же даты, но через год. Мы начали экстренно искать новое жилье. Понятное дело, что большая часть уже была не доступна, пришлось выбирать из очень дешевого, но сомнительного жилья и чем-то подороже.
Таким образом у нас оказались апартаменты с двумя спальнями и двориком в Пунате.

<a href="img/IMG_6812.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6812.jpg"/>
</a>

До моря минут 10 пешком. Рядом распологался кемпинг. Глядя как в нем отдыхают немцы и австрийцы, мы загорелись идеей аренды домика на колесах на следующий летний отпуск на море.

Договорились с хозяйкой, что поселимся в 11 утра.
Приехали, вещи выгрузили и пошли искать где бы пообедать. Пообедали ризотто с морепродуктами в ресторанчике на берегу моря, а ребенок конечно же возжелал пиццу. Как мы не уговаривали, что в Италии она ей еще наедоест, она была непреклонна.

<a href="img/IMG_6725.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6725.jpg"/>
</a>

И наконец-то после обеда занырнули в прохладу Адриатического моря.
Оно ужасно соленое. В глаза если попадет - очень щиплет.

Пляж - бетонные плиты. Я люблю такое. У нас в семье нет малышей, которым по возрасту положено капать траншеи в песке и строить замки, моя дочь или читает или лазит по камням. Поэтому довольны все.

<a href="img/IMG_6796.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6796.jpg"/>
</a>

<a href="img/IMG_6821.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6821.jpg"/>
</a>

Нашли супермаркет недалеко от апартаментов. Ужинали гречкой, сосисками и салатом на свежем воздухе. Потом гуляли по набережной, любовались закатом.

<a href="img/IMG_6776.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6776.jpg"/>
</a>

Весь следующий день провели на море. Домой сходили только на обед и послеобеденную сиесту.

<a href="img/IMG_6949.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6949.jpg"/>
</a>

Ужинали в ресторанчике на набережной.

А утром снова дивинули в путь. От Пуната до Ровиня 145 километров.

<h2>Ровинь</h2>

В Ровинь вернулись как домой: все родное и знакомое. Мы в прошлом году здесь отдыхали и нам очень понравился этот город. С удовольствием приехали сюда и в этом году. Жилье присмотрели поближе к прошлогоднему - очень удобное расположение: рядом магазины и до моря минут 15 пешком.
Много апартаментов сдают в районе старого города. Да, удобно жить рядом с историческим центром, где много кафешек и ресторанчиков, но купаться там фактически негде, да и с магазинами в центре не очень.

Итак, мы арендовали апартаменты с одной спальней. При заезде нас ждал подарок от хозяев: ракия - хорватская самогонка. На следующий день мы ее осмелились попробовать. Это просто огненное мерзкое пойло. Ужасно крепкое и по вкусу спирт чистый. Не любители мы таких напитков.

Во дворе увидели машину на чешских номерах. На чешско-русско-украинском пообщались с ее хозяином - приехал на две недели, собирается на яхте плавать со знакомым капитаном и на велосипеде кататься. Велосипед привез с собой.

Ездили в аэропорт - встретили нашу подругу из Киева.

Ели обалденно вкусное мороженое. Я вот просто обожаю мороженое в Италии и Хорватии. Вкуснее я нигде не ела. Особенно мятное.

Обедали в ресторанчике на берегу, где ели в прошлом году. Недорого и вкусно, но много народу. Надо успеть занять столик до 13 часов иначе потом аншлаг и можно приходить после 15 часов или ждать в очереди.

Вечером за ужином отпраздновали День Независимости Украины блюдом морепродуктов и вином. Ну и приезд подруги, конечно же.

Следующий день провели в Ровини: днем купались, загорали, а вечером гуляли по городу.
Красивое море в лучах заходящего солнца.

<a href="img/IMG_6837.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6837.jpg"/>
</a>

<a href="img/IMG_6864.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6864.jpg"/>
</a>

Как же это чудесно, выйти из моря и растянуться на теплых плитах на солнце.

<a href="img/IMG_0060.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_0060.jpg"/>
</a>

Это был последний день в Хорватии, в ней мы провели 7 дней. Дальше наш путь лежал в Италию.

Ссылка на видео в нашей поездки:
https://www.youtube.com/watch?v=GpmtepaEzC8
