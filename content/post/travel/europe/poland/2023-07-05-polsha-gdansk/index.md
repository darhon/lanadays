+++
banner     = "banners/2023-07-05-polsha-gdansk.jpeg"
title      = "Подорож по Польщі з трирічкою: Гданськ"
author     = "Lana"
date       = "2023-07-05"
categories = ["travel"]
tags       = ["europe", "poland"]
+++

<p>Отже 4 день нашої поїздки. Після сніданку поїхали у Гданськ. Малий вже скучив за дорогою і питав, коли далі поїдемо?</p>

<!--more-->

<p>Незважаючи на ремонт дороги біля Мальборку за годину ми вже шукали місце для паркування у Гданську. В Гданську ми теж вже були у 2018 році але мені тоді не вдалося помочити ніжки у Балтійському морі (хоча це давня мрія), бо було досить прохолодно. Тож в цей раз я собі запланувала прогулку по берегу моря, навіть купальник с собою взяла. Спойлер: мрія залишилась мрією. Мабуть треба втретє туди їхати. Але все по порядку. </p>


<p>Першим ділом в Гданську ми покатали малого на каруселі. Знаєте така, двохповерхова, з рухаючимися кіньми та каретами? Малий спочатку плакав, бо боявся сідати на коня, потім плакав, бо не хотів виходити з каруселі. Життя бентежне у трирічки. </p>
<figure>
<a href="img/marry-go-round.jpeg" target="_blank">
  <img width="900" height="675" border="0" align="center"  src="img/marry-go-round.jpeg"/>
</a>
<figcaption>Махає "привіт", а очі перелякані</figcaption>
</figure>
<br/> 

<p>Покатались на оглядовому колесі, подивились як піднімають та опускають міст (півгодини чекали, поки опуститься, щоб пройти по ньому), побродили по вулицях.</p> 
<figure>
<a href="img/denis.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/denis.jpeg"/>
</a>
<figcaption>Оглядове колесо</figcaption>
</figure>
<br/> 
<figure>
<a href="img/city.jpeg" target="_blank">
  <img width="900" height="675" border="0" align="center"  src="img/city.jpeg"/>
</a>
<figcaption>Місто з висоти</figcaption>
</figure>
<br/> 
<figure>
<a href="img/bridge.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/bridge.jpeg"/>
</a>
<figcaption>Той самий міст</figcaption>
</figure>
<br/> 
<p>Дуже багато туристів, спека, місто активно розбудовують, тож довкола доволі шумно та запорошено. Пробіглись по центру міста</p>
<figure>
<a href="img/neptune.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/neptune.jpeg"/>
</a>
<figcaption>Фонтан Нептун</figcaption>
</figure>
<br/> 
<figure>
<a href="img/St-Mary-Church.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/St-Mary-Church.jpeg"/>
</a>
<figcaption>Базиліка Внебовзяття Пресвятої Діви Марії</figcaption>
</figure>
<br/> 

<p>Пообідали в рибному ресторані і поїхали в готель, бо малий вже куняв над тарілкою. Тож моя мрія поїхати на Балтійське море цього разу не здійснилась. Малий поспав в машині, а ввечері ми знову гуляли околицями замка поки не почалася гроза.</p>
<figure>
<a href="img/me.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/me.jpeg"/>
</a>
<figcaption>Фотка на шляху до автівки</figcaption>
</figure>
<br/> 
