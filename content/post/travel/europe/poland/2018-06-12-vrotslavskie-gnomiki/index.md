+++
banner     = "banners/2018-06-12-vrotslavskie-gnomiki.jpg"
title      = "Вроцлавские гномики"
author     = "Lana"
date       = "2018-06-12"
categories = ["travel"]
tags       = ["europe", "poland"]
+++

Шел 4 день нашего путешествия по Польше. За предыдущие дни мы успели увидеть окраину [Варшавы][1], [Замок Мальборк][2] и [Гданьск][3]. В этот день нам предстояло преодолеть около 450 км и опробовать платные автобаны, движение по которым нам экономило 100 км.

<!--more-->

## Дорога Маленин-Вроцлав

![](img/wind_0.jpg)

![](img/wind_1.jpg)

Как я писала ранее, ехать по польским автобанам &#8211; сплошное удовольствие. Едешь 120-140 км/час и даже не замечаешь скорости. Зато на обочине были замечены рабочие, которые косили траву вдоль дороги. Поэтому радуют не только ровные дороги, но и ухоженные обочины вдоль них.

![](img/nature_0.jpg)

С платной дорогой оказалось все просто: заезжаешь &#8211; берешь талончик, выезжаешь &#8211; платишь по талончику, сколько проехал километров. На втором платном участке ставка одна для всех легковых автомобилей &#8211; 10 злотых на въезде и 10 на выезде. Вот этот хитрый момент для меня остался за гранью понимания.

Получилось так, что эти 450 километров дороги растянулись на целый день: мы останавливались выпить кофе, купить мороженое, пообедать и помимо этого еще несколько санитарных остановок. Обедали мы где-то на средине пути в Restauracja GOŚCINIEC SOBAŃSKICH (где-то возле Познани).

![](img/horse.jpg)

Дочери дорога далась нелегко. Все таки 9 часов в машине высидеть &#8211; это то еще испытание для 8 летнего ребенка. Час она играла на планшете, полчаса ушло на мороженное, час читала книгу, еще пару часов спала. Остальное время смотрела в окно. Большой плюс в том, что ребенок у нас не нытик, она стойко переносит такие моменты вынужденной скуки.

До Вроцлава еще 70 км

![](img/road.jpg)

Почти приехали:

![](img/wroclaw_0.jpg)

## Отель Gościniec Pod Furą

Наш отель Gościniec Pod Furą находился в 30 км от Вроцлава. Попали в час пик, поэтому попали в небольшую тянучку на окружной дороге. Само название гостиницы, которые мы для себя поняли как &#8220;Под фурой&#8221;, вызвало у нас много шуток. Было уже около 8 вечера, когда мы наконец-то доехали до нее. Оказалось, что название к фуре не имеет никакого отношения, а слово &#8220;furą&#8221; переводится как &#8220;телега&#8221;, которая висела в ресторане под потолком. Но мы так и не решились сесть за столик под ней.

В зале на потолке красивые рисунки из &#8220;Игры престолов&#8221; и просто фэнтезийные мотивы.

![](img/hotel_1.jpg)

![](img/hotel_2.jpg)

![](img/hotel_3.jpg)

![](img/hotel_4.jpg)

После ужина в ресторане отеля мы отправились спать, а с утра нас ждал Вроцлав.

## Паркинг во Вроцлаве

После завтрака мы ребенком прошлись вдоль по улице, на поле напротив отеля сфотографировали маки и пейзажи.

![](img/mac.jpg)

![](img/nature_1.jpg)

А потом на одном из заборов частного дома увидели смешную табличку:

![](img/pies.jpg)

Пока мы рассматривали табличку и высматривали злую быстроногую собаку из дома вышел пожилой мужчина. На польско-украинском мы рассказали откуда мы, где уже были и куда едем. Не знаю, понял он нас или нет, но он искренне и много улыбался. Вот эта встреча зарядила нас позитивом на весь день.

До Вроцлава мы доехали за полчаса. Нашли парковку и стали изучать местный паркомат. Это был будний день и надо было заплатить за парковку. Но как? Это нам предстояло узнать. Хорошо, что возле паркомата нам встретилась англоговорящая девушка, которая доходчиво объяснила несколько нюансов: во-первых, паркомат принимает только монеты, во-вторых, за парковку надо обязательно платить, так как в центре часто ездит полиция и эвакуирует машины, в-третьих, парковка в торговом центре &#8220;Galeria Dominikańska&#8221; &#8211; дешевле. Вообще как ни странно, но эта девушка &#8211; манна небесная, редко кого мы встретили говорящего на английском. Как ни странно.

Поехали мы в подземный паркинг этого ТРЦ. На въезде взяли талончик, а как потом платить и выезжать &#8211; не понятно. Словили молодого человека на парковке, он нам все рассказал и показал. Я очень удивилась, как это мы так хорошо поняли друг друга. И только когда мы вышли, я осознала, что он говорил с нами на украинском. А я радовалась, что наконец-то начала понимать польский. Бывает.

## Вроцлавские гномики

![](img/dwarf_3.jpg)

Вроцлавские гномы &#8211; необычная и очень интересная достопримечательность города. По всему городу расставлены бронзовые статуетки гномиков разных профессий и занятий. У каждого &#8211; своя история и имя. В 2001 году появился в городе первый папа-гном:

![](img/dwarf_papa.jpg)

У них даже есть свой сайт [Wroclaw Dwarves](http://krasnale.pl), где можно на карте посмотреть месторасположение гномиков:

![](img/dwarf_map.png)

Дети с удовольствием искали гномов и даже устроили свое соревнование, кто первым увидит больше гномов.

![](img/dwarf_1.jpg)

![](img/dwarf_2.jpg)

![](img/dwarf_4.jpg)

![](img/wroclaw_4.jpg)

## Прекрасные улочки Вроцлава

![](img/wroclaw_2.jpg)

![](img/wroclaw_3.jpg)

![](img/wroclaw_1.jpg)

По прибытию не стали нарушать традицию и пошли выпить вкусного кофе в Café Vincent. Какая здесь выпечка, десерты и завтраки!!! Произведения искусства просто! Интерьер тоже соответствующий, очень уютно.

![](img/vincent_1.jpg)

![](img/vincent_0.jpg)

Вот, кстати, пример современного торгового центра, который идеально вписался в историческую часть города. Никаких билбордов, рекламы и вывесок.

![](img/trc.jpg)

Проходили по парку, восхитились, какой он ухоженный и с Wi-Fi.

![](img/park_wroclaw.jpg)

Один из подземных переходов украшен плиткой. Её можно рассматривать бесконечно.

![](img/cross_0.jpg)

![](img/cross_1.jpg)

Мы же направили свои стопы к Тумскому острову, где взял свое начало город Вроцлав.

![](img/wroclaw_5.jpg)

![](img/wroclaw_6.jpg)

![](img/wroclaw_7.jpg)

![](img/wroclaw_8.jpg)

![](img/wroclaw_9.jpg)

![](img/wroclaw_10.jpg)

![](img/wroclaw_11.jpg)

![](img/wroclaw_12.jpg)

Обедали в грузинском кафе. Хачапури и тархун были хороши.

![](img/georgia_0.jpg)

![](img/georgia_1.jpg)

Университет:

![](img/wroclaw_14.jpg)

![](img/university.jpg)

Прошлись по набережной. Город очень красивый и уютный.

![](img/wroclaw_13.jpg)

![](img/wroclaw_16.jpg)

![](img/wroclaw_17.jpg)

![](img/wroclaw_18.jpg)

![](img/wroclaw_19.jpg)

![](img/wroclaw_20.jpg)

Ближе к вечеру уставшие и довольные поехали в отель, где под взгляды героев книг Джорджа Мартина с потолка мы поужинали и отправились спать.

![](img/hotel_0.jpg)

 [1]: ../2018-05-29_polsha_za_7_dnej/
 [2]: ../2018-06-04-zamok-malbork/
 [3]: ../2018-06-06-den-v-gdanske/
