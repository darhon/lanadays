+++
banner     = "banners/2023-07-02-polsha-leshno.jpeg"
title      = "Подорож по Польщі з трирічкою: Лешно"
author     = "Lana"
date       = "2023-07-02"
categories = ["travel"]
tags       = ["europe", "poland"]
+++

<p>Ну що, друзі, знову почнемо писати-читати? Дуууже довго не писала, скучила за цим процесом та навіть вже забула, яке це задоволення: побачене загортати у слова.
Отже маю чудову нагоду знову вдихнути життя в свій блог: повернулися із тижневої автоподорожі Польщею, маю багато фоток та бажання поділитися.
Тож розпочнемо =) </p>

<!--more-->

<p>В Чехії 5-6 липня два святкові дні і ми скористалися нагодою, відправили доньку в табір, а самі запланували невеличку автомандрівку до Польщі на 8 днів. Ну як невеличку - 1700 км проїхали та 5 польських міст побачили. Молодшому учаснику автопробігу 2,9 років, тож криза трьох років в “розгарі” і я не очікувала від тієї поїздки нічого доброго. Але думка про те, що на свята та у вихідні дні знову буде “день сурка”: поспав-поїв-майданчик, то краще вже їхати. Все ж таки зміна обстановки, нові місця побачити та й готувати-прибирати не треба =) </p>

<p>Отже склали маршрут: Прага - Лешно - Мальборк - Гданськ - Познань - Вроцлав - Прага. </p>

<p>І в день “х” після сніданку виїхали. Задали першу точку в навігатор, а він видає 600+ км дороги. Еее ні, думаємо, щось не те. Почали розбиратися, виявилося, що забронювали готель десь аж під Варшавою. Як так сталося? Мабудь дуже у відпустку поспішали, не проконтролювали місцезнаходження. Вирішили пошукати інший готель поки будемо їхати. Отже на технічній зупинці, поки малий скакав на травці, забронювали готель у Лешно (три рази місцезнаходження перевіряли).</p>

<figure>
<a href="img/zastavka.jpeg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/zastavka.jpeg"/>
</a>
<figcaption>Зупинка в Польщі</figcaption>
</figure>
<br/>

<p>Напишу, що набрала малому в дорогу з іграшок: шнуровку, ігру на наліпках “паркинг”, магнитну дошку для малювання, альбом та олівці, кубік-рубіка, машинки, розмальовки, pop-it, книжку з наліпками. Із їжі малому взяла пиріжки з яблуком, банани, кукурудзяні палочки, фруктові батончики, пюре. Готувались морально до того, що малий не захоче сам позаду сидіти, почне вередувати і хтось повинен буде пересісти до нього. Бо ж зазвичай поруч з ним сидить старша і виконує його примхи. Але в цей раз всю дорогу він сам себе розважав, дивився у вікно, а ми лише подавали йому іграшки, їжу, водичку. Частину дороги він проспав. Тож на півшляху зробили зупинку, випили кави, поїли і дали змогу сину хвилин 20 побігати. За 4,5 години легко доїхали до першого міста - Лешно.</p> 

<figure>
<a href="img/hotel.jpeg" target="_blank">
  <img width="450" height="600" border="0" align="center"  src="img/hotel.jpeg"/>
</a>
<figcaption>Кінь перед готелем</figcaption>
</figure>
<br/>

<p>Обирали місто для ночівлі просто: приблизно посередині шляху від Праги до Гданська. Довше їхати було би вже напряжно для малого, а так - півдня їхали, півдня гуляли містом. І обідали вже в Лешно. Симпатичний готель в приміщенні колишніх стайней. Цікавий інтерєр, багато картин та фотографій коней, старовинні меблі. Люблю місця з історією.</p>

<figure>
<a href="img/hotel_1.jpeg" target="_blank">
  <img width="450" height="600" border="0" align="center"  src="img/hotel_1.jpeg"/>
</a>
<figcaption>Интер'єр в готелі</figcaption>
</figure>
<br/>

<p>Після обіду гуляли містом. На центральній площі є цікавинки, що прикрашають вулиці: парасольки та повітряні кулі над головою, а також лазалки для дітей, кав'ярні, цукрарні, навіть котяче кафе є. А в приміщенні ратуші - бібліотека. Людей багато, таке враження, що половина населення міста недільний вечір проводіть в центрі. Хоча мабуть так і є, містечко маленьке, чим ще розважитись?</p> 

<figure>
<a href="img/leshno_1.jpeg" target="_blank">
  <img width="450" height="600" border="0" align="center"  src="img/leshno_1.jpeg"/>
</a>
<figcaption>Вулиці Лешно</figcaption>
</figure>
<br/>

<figure>
<a href="img/leshno_2.jpeg" target="_blank">
  <img width="450" height="600" border="0" align="center"  src="img/leshno_2.jpeg"/>
</a>
<figcaption>Вулиці Лешно</figcaption>
</figure>
<br/>

<figure>
<a href="img/leshno_3.jpeg" target="_blank">
  <img width="450" height="600" border="0" align="center"  src="img/leshno_3.jpeg"/>
</a>
<figcaption>Вулиці Лешно</figcaption>
</figure>
<br/>

<p>З нашею вечерею забавно вийшло: ресторан при готелі, за обідом нам сказали, що працюють до 21 години. Але коли ми прийшли повечеряти о 19:40, то виявилося, що кухня вже не працює. Але так як кухар ще не пішов додому (офіціант запропонував перевірити), то ми змогли замовити вечерю. Як тільки перед нами поставили нашу їжу ми побачили як персонал ресторану пішов додому =) Залишився тільки один офіціант і він же за суміцництвом працівник рецепшена в готелі. От вам маленьке містечко. Після вечері - прогулялись ще біля готелю та зайшли на дитячий майданчик.</p> 

<p>Ось і день промайнув. Пішли в готель спати і зранку поїхали далі.</p> 


