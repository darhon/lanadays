+++
banner     = "banners/2023-07-04-polsha-malbork.jpeg"
title      = "Подорож по Польщі з трирічкою: Мальборк"
author     = "Lana"
date       = "2023-07-04"
categories = ["travel"]
tags       = ["europe", "poland"]
+++

<p>Вранці поснідали, зібрали свої лахи і поїхали далі. Знову за 4 години легкої дороги (досі не вірю, що це було так легко та безпроблемно) дісталися Мальборка. Це наш прихисток на 3 ночі.</p>

<!--more-->

<p>Перед поселенням пообідали в ресторані при готелі. Маленький сімейний готель на декілька номерів на дорозі, проте через дорогу розташований замок Мальборк (Марієнбург). Ми вже були в цьому замку в 2018 році, тоді він дуже нас вразив адже це найбільший цегляний замок в світі з багатою та цікавою історією. Тож вирішили знову його відвідати. Після денного сну сина пішли гуляти містом: пройшлись набережною, походили біля замку, дійшли до центру.
Від готелю до замку веде чудовий місточок через річку Ногат.</p>

<figure>
<a href="img/bridge.jpeg" target="_blank">
  <img width="900" height="675" border="0" align="center"  src="img/bridge.jpeg"/>
</a>
<figcaption>Міст через річку Ногат</figcaption>
</figure>
<br/> 

<p>Біля замку вздовж річки веде дуже гарна набережна з велодоріжкою.</p>

<figure>
<a href="img/zamek_13.jpeg" target="_blank">
  <img width="900" height="675" border="0" align="center"  src="img/zamek_13.jpeg"/>
</a>
<figcaption>Набережна: ходити, їздити, бігати</figcaption>
</figure>
<br/> 
<figure>
<a href="img/zamek_4.jpeg" target="_blank">
  <img width="900" height="675" border="0" align="center"  src="img/zamek_4.jpeg"/>
</a>
<figcaption>Малий залюбувався</figcaption>
</figure>
<br/> 

<p>Очевидно, що малий втомився сидіти в автівці, тож почав носитися вулицями, забігати в усі двері, залазити всюди, куди міг потягнутися. Коли наше терпіння дійшло кінця ми повернулися в готель. Така сурова правда мандрівок з дитиною =) Вирішили, що в замок підемо вранці. </p>

<figure>
<a href="img/zamek_2.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/zamek_2.jpeg"/>
</a>
<figcaption>Восьмиметрова статуя Мадонни з немовлям</figcaption>
</figure>
<br/> 
<figure>
<a href="img/madonna.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/madonna.jpeg"/>
</a>
<figcaption>Мадонна з немовлям</figcaption>
</figure>
<br/> 
<figure>
<a href="img/zamek_3.jpeg" target="_blank">
  <img width="900" height="675" border="0" align="center"  src="img/zamek_3.jpeg"/>
</a>
<figcaption>Замок</figcaption>
</figure>
<br/> 

<p>Зранку пішли в замок Мальборк. На шляху зустріли Маріанека - бронзового лицаря, офіційного талісмана Мальборка. Виявилося, що перші три лицарі зявилися в 2020 році. Тепер вони зявляються у інших видатних місцях міста також. Він такий гарненький, цей лицар. Але ми не уважні і не побачили би його, якби не один пан, який стояв і з усіх ракурсів фоторграфував лицаря. Чим і привернув нашу увагу. </p>
<figure>
<a href="img/knight.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/knight.jpeg"/>
</a>
<figcaption>Лицар Маріанек з компасом</figcaption>
</figure>
<br/> 

<p>В касі взяли аудіогід на рідній мові та пішли. </p>
<figure>
<a href="img/zamek_5.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/zamek_5.jpeg"/>
</a>
<figcaption>Щасливий власний квиточка до замку</figcaption>
</figure>
<br/> 

<p>Рухалися по замку в своєму темпі, десь затримуючись, дещо пропускаючи. І це, скажу я вам, набагато краще, ніж ходити з екскурсією. </p>
<figure>
<a href="img/zamek_6.jpeg" target="_blank">
  <img width="900" height="675" border="0" align="center"  src="img/zamek_6.jpeg"/>
</a>
<figcaption>Руїни замку</figcaption>
</figure>
<br/> 
<p>Син ходив зі мною за ручку, розглядав, питав, щось я йому розповідала. Але години через 3 малий втомився: почав все щупати, бігати, тож довелося його вести на обід та спати. Хоча ми пройшли лише половину замку. </p>
<figure>
<a href="img/zamek_8.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/zamek_8.jpeg"/>
</a>
<figcaption>Замок</figcaption>
</figure>
<br/> 
<figure>
<a href="img/zamek_9.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/zamek_9.jpeg"/>
</a>
<figcaption>Замок</figcaption>
</figure>
<br/> 
<figure>
<a href="img/zamek_10.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/zamek_10.jpeg"/>
</a>
<figcaption>Замок</figcaption>
</figure>
<br/> 
<figure>
<a href="img/zamek_11.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/zamek_11.jpeg"/>
</a>
<figcaption>Замок</figcaption>
</figure>
<br/> 
<figure>
<a href="img/zamek_12.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/zamek_12.jpeg"/>
</a>
<figcaption>Виглядаємо з віконечка</figcaption>
</figure>
<br/> 

<p>Другу половину вже пробігли бігом на вихід. Чоловіка залишили насолоджуватись замком та тишею. </p>

<figure>
<a href="img/tree.jpeg" target="_blank">
  <img width="675" height="900" border="0" align="center"  src="img/tree.jpeg"/>
</a>
<figcaption>Сплетіння</figcaption>
</figure>
<br/> 

<p>Після сну пішли в магазин Biedronka. Для мене цей магазин відомий тим, що чехи їздять там закупатися, там дешеві продукти і є українські товари. Мені було дуже цікаво подивитись, що ж воно таке той магазин. Із цікавого для себе побачила львівську каву, деякі цукерки (типу пташиного молока та чорнослива у шоколаді), крупи. Але на нього треба багато часу: продукти стоять впереміш і щоб щось знайти або побачити, то треба все передивлятись. Взяли на вечерю сир кисломолочний та йогурти, бо щось за два дні трохи втомилися від ресторанної їжі. Ну і стандратно перед сном ще трохи погуляли по місту.</p>