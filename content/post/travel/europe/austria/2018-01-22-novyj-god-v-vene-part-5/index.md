+++
banner     = "banners/2018-01-22-novyj-god-v-vene-part-5.jpg"
title      = "Новый год в Вене (часть 5): Дом Хундертвассера, Пратер и новогодняя ночь на площади возле Венской Ратуши"
author     = "Lana"
date       = "2018-01-22"
categories = ["travel"]
tags       = ["europe", "austria", "vienna"]
+++

О, этот долгожданный день года, когда кажется, что с боем курантов начнется совсем новая жизнь, а все неугодное останется в прошлом году. Но чудес не бывает, к сожалению.

В предыдущей [части](https://lanadays.com/post/travel/europe/austria/2018-01-17-novyj-god-v-vene-part-4/) мы посетили Шёнбруннский зоопарк и парк Шёнбрунн. А в этот день мы увидели Дом Хундертвассера (Hundertwasserhaus), Пратер (Prater) и закончили этот год на площади возле Венской Ратуши (Wiener Rathaus). Но до вечера еще далеко, ведь день только начался.

<!--more-->

## Дом Хундертвассера

После вкусного завтрака мы решили отправиться в Дом Хундертвассера. Как обычно, после завтрака, купили билет на метро и поехали. Мы вышли на станции метро Rochusgasse (линия U3) и пошли в сторону этого интересного дома. По пути мы много чего интересного увидели, рассматривали окна, балконы и витрины. Людей на улицах было мало.

Инсталляция из множества скворечников:

![](img/skvorechniki.jpg)

Вот так выглядят улицы Вены:

![](img/street_1.jpg)

![](img/street_2.jpg)

![](img/street_3.jpg)

А в такой штуке возят деток:

![](img/cycle.jpg)

Вот и сам этот дом:

![](img/house.jpg)

Чем примечательно это место? Это необычный жилой дом в Вене, который построен по проекту художника Фриденсрайха Хундертвассера. Художник был сыном австрийца и еврейки. Отец его умер, когда ему не было еще и месяца и мать его воспитывала одна. Фридрих &#8211; это его настоящее имя, которое ему пришлось сменить на немецкое Фриденсрайх, чтобы избежать преследования нацистов. Он не любил прямые углы, прямые линии и блеклость. Фридрих считал, что люди несчастны живя в одинаковых домах, поэтому каждый волен раскрасить свой дом так, как ему хочется. Не только внутри, но и снаружи. Дом Хундертвассера построен максимально ярким и красочным и без прямых углов.

Художник всегда носил разные носки и когда люди его спрашивали, почему он носит разные носки, он отвечал: &#8220;А почему, собственно, вы носите одинаковые?&#8221;. Вот таким неординарный и интересным человеком был этот художник.

Туристы и экскурсоводы любят это место, поэтому мы быстренько его осмотрели и пошли дальше по улице в сторону Дунайского канала.

![](img/dunaikanal.jpg)

![](img/dunai_kanal.jpg)

Прогулялись вдоль канала и вышли к парку Пратер (Prater).

## Парк Пратер

Это большой парк с зеленой зоной и аттракционами. Так как мы были там в канун нового года, то в парке было много рождественских лавочек с едой, глинтвейном и сувенирами. Как оно все пахнет! Мясом на мангале,

булочками с корицей и глинтвейном (glühwein). Но мы решили повременить с едой и сначала посмотреть на аттракционы.

Самыми примечательными были два колеса обозрения и Prater Tower. Prater Tower &#8211; это самая высокая в мире цепочная карусель. Вас поднимает на высоту 117 метров и с нее начинается головокружительный полет вниз по спирали.

![](img/prater_tower_1.jpg)

Колесо обозрения (Wiener Riesenrad) является одним из символов Вены. На высоте почти 65 метров открывается чудесный вид на город. Но очередь к нему длиннющая. В Пратере есть еще одно колесо обозрения &#8211; поменьше и открытые кабинки, но зато без очереди. В тот день был сильный ветер и ощущения в раскачивающейся на ветру кабинке весьма неоднозначные.

![](img/vienna_1.jpg)

Последнее из аттракционов, что мы посетили, была комната страха. Ничего особенного. Пауки на голову не падали и за руку никто не дергал. Ребенок даже не испугался.

Пообедали мы тут же: купили гуляш и тыквенный супчик на рождественской ярмарке (заплатили 14.50 евро). Дальше было решено встретиться со знакомыми и послушать от местных аборигенов, как им живется в Вене. Для этого мы поехали на Марийку. Я уже писала, что это шоппинг-улица Mariahilferstrasse, на которой много магазинов и кафе, так вот, местные называют ее &#8220;Марийка&#8221;.

![](img/mariyka.jpg)

## Традиции в Австрии

Для встречи мы выбрали Starbucks. Просто идеальное место, где тепло, есть вкусный кофе и интернет. Мы взяли себе кофе, а дочери чай. Еще я решила попробовать чизкейк &#8220;Нью-Йорк&#8221;. Весьма посредственный, на самом деле. За два кофе, чай и чизкейк мы отдали 14.40 евро.

![](img/starbucks.jpg)

Мы пили кофе и общались с друзьями. Что интересного я услышала о Вене и об Австрии в целом? В новогоднюю ночь местными жителями принято брать шампанское и бокалы и к полуночи выходить на площадь Стефана (Stephansplatz). В полночь открывают шампанское, выпивают по бокалу и расходятся по домам спать. До утра редко кто сидит и гуляет, как-то не принято.

На сочельник каждая хозяйка обязательно готовит рождественское ванильное печенье в виде полумесяцев (Vanillekipferl).

![](img/vanillekipferl.jpg)

Если идут в гости, то обязательно берут с собой это печенье. Бывает, что за столом встречаются три поколения и каждая семья приносит свое печенье.

При встрече австрийцы обмениваются рукопожатием и приветствуют друг друга словами: Gruss Gott (буквально &#8211; &#8220;приветствую бога&#8221;) или Gruss dich (&#8220;приветствую вас&#8221;).

Идя в гости к кому-то обязательно берут с собой подарки хозяевам: хозяйке &#8211; цветы (в Австрии принято дарить четное количество), а хозяину &#8211; бутылочку вина.

После кофе в Starbucks мы пошли на площадь Марии Терезии. Сама площадь находится между двумя музеями: Музеем естествознания (Naturhistorisches Museum Wien) и Музеем истории искусств (Kunsthistorisches Museum Wien). К сожалению, в этот раз мы в музеи не попали, но для себя я отметила пару мест, куда я хочу обязательно попасть в следующую нашу поездку в Вену. И эти два музея как раз возглавляют этот список.

На площади очень большая рождественская ярмарка, где мы продолжили наше общение за кружечкой глинтвейна.

![](img/maria_teresa.jpg)

Эти засушенные фрукты и овощи удивительно пахнут корицей:

![](img/fruits.jpg)

Глинтвейн и пунш на ярмарке продают в керамических чашках. Мне эта идея очень понравилась: мусора минимум, никаких пластиковых и бумажных стаканчиков не валяется. Первый раз покупая глинтвейн мы заплатили залог за чашку 3 евро, потом с этой чашкой можно бродить от одной ярмарки к другой, пробуя напитки в разных лавочках. Когда надоест &#8211; просто вернуть чашку и получить свой залог обратно.

В 2012 году я привезла свою первую чашку с Вены, а в этом году мы привезли еще три чашки. У каждой лавочки с горячим вином свой стиль чашки: где-то белые с красной надписью, где-то в виде поросенка, а на площади Марии Терезии были чашки в виде Санта-Клауса. Очень милые чашки! И насколько приятнее пить пунш из керамики, чем из пластика. Очень многие носят эти чашки с собой, некоторые даже на пояс их вешают, чтобы руки освободить. Мы даже в метро видели пассажиров с чашками. Это так необычно и интересно.

Мы нагулялись и устали, а на часах всего 5 вечера. Купили ребенку на площади хот-дог и решили поехать в отель и часов до 9 вечера отдохнуть, чтобы набраться сил перед новогодней ночью.

## Новогодняя ночь на площади возле Венской Ратуши.

Друзья нас предупредили, что лучше не идти на площадь Стефана, где будет просто уйма народа, а посоветовали нам пойти под Ратушу. Для начала мы решили плотно поужинать в Rosenberger. Благо дело в вечер накануне нового года этот ресторан работал в обычном режиме.

Еще при поселении в отель нам сразу выдали карту мероприятий и концертов в новогоднюю ночь. Там было аж целых 10 пунктов и можно было просто <del datetime="2018-01-21T21:47:13+00:00">идти за белым кроликом</del> ходить от одной точки к другой, ведь большинство находилось в центре города. Мы так и поступили. В одном месте на сцене пел хор, в другом &#8211; играла рок-группа, в третьем &#8211; из колонок звучали хиты, а молодежь лихо отплясывала. Так мы бродили туда-сюда и пили глинтвейн (ребенок пил детский пунш). Людей на улицах становилось все больше и больше, но пьяных и агрессивных мы не видели. Так как фейерверки и петарды запрещены в Вене, то на одной из улиц мы увидели, как полиция остановила группу молодежи и выуживала у них запрещенные предметы.

![](img/vienna_4.jpg)

![](img/vienna_3.jpg)

Мы так устали за день, что только наше упрямство и яблоко в карамели, которое грызла дочь, нас удерживало от того, чтобы плюнуть на все и пойти спать.

![](img/rathouse.jpg)

Ближе к 12 ночи мы пришли на площадь возле Ратуши. Около нее стояла большая сцена, на которой шел концерт, а вокруг очень большой рождественский ярмарок. И все такое яркое и сияющее. Организаторы позаботились не только о еде и горячих напитках для гостей, но и естественные потребности тоже не забыли: чуть дальше за площадью стоял ряд биотуалетов. Несмотря на то, что людей на площади много и возле туалетов всегда стоит очередь (мы стояли минут 7 от силы), туалеты чистые и с туалетной бумагой.

Когда часы на ратуше пробили полночь, небо взорвалось салютами, которые продолжались 5 минут. После салюта мы пошли к метро. Оказалось, что очень многие поспешили в свои кроватки. На входе в метро образовалась толпа, но все цивилизованно и не спеша спускались под землю. Не знаю, что больше повлияло: воспитание или тот факт, что в праздники метро работает круглосуточно, но никакой давки и суеты не наблюдалось. Вагоны метро тоже штурмом никто не брал и не толкался. Люди видели, что не помещаются в вагон и спокойно оставались стоять на платформе, ведь согласно табло через 4 минуты будет следующий поезд.

Вот так мы провели день 31 декабря и новогоднюю ночь.

У нас оставался еще один день в Вене. Продолжение следует.
