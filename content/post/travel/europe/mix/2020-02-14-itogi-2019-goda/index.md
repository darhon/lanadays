+++
banner     = "banners/2020-02-14-itogi-2019-goda.jpg"
title      = "2019 год: 6 стран и 6 чешских городов"
author     = "Lana"
date       = "2020-02-14"
categories = ["travel"]
tags       = ["europe", "malta", "croatia", "czechrepublic", "denmark", "faroe", "italy", "slovakia"]
+++

<p>2019 год был самым насыщенным в плане путешествий. Он был идеально таким, про который можно смело сказать, что это был счастливый год. </p>

<p>Мы посетили 6 стран (Словакия, Бельгия, Италия, Хорватия, Мальта, Дания (Фарерские острова)), 6 чешских городов (Брно, Пльзень, Ческе Будеёвице, Чески-Крумлов, Тшебонь, Кутна Гора), 1 Metalfest open air и 1 концерт Metallica. </p>

<!--more-->

<p>Полезно все таки подводить итоги, оглядываться и переводить полученные эмоции и впечатления в цифры. А то бывает сидишь, смотришь каналы путешственников на  YouTube и кажется, что все такие молодцы, а мы все дома сидим. А потом посчитаешь, сколько всего было за год и оказывается, что исколесил Европу неплохо так. </p>

<p>Погнали вспоминать! </p>

<h2>Январь и февраль</h2>
<p>В январе мы взяли билеты на поезд и через каких-то 4 часа были уже в <strong>[Братиславе](https://lanadays.com/post/travel/europe/slovakia/2019-01-13-dva-dnya-v-bratislave/)</strong>. Очень удобно: сидишь в тепле, уюте, читаешь книгу, жуешь булочки или смотришь в окно. В Братиславе запомнились улочки города и десерт из огурца, шоколада и мороженого в “Летающей тарелке” (UFO Reštaurácia).</p>
  <figure>
  <a href="img/IMG_9733.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_9733.jpg"/>
  </a>
  <figcaption>Чайки и тарелка в Братиславе</figcaption>
  </figure>
  <br/>
<p>Поначалу ощущение качки не давало расслабиться (не много, ни мало, а 95 метрв над Дунаем), но это ощущение быстро проходит.
</p>
<p>Также это наш первый опыт аренды хостела. Наша комната была на троих, а вот санузел должны были делить с соседним номером. Но там никто не жил, поэтому все было нашим. Кухня и гостиная приятно удивили.
  <figure>
  <a href="img/IMG_9813.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_9813.jpg"/>
  </a>
  <figcaption>Гостиная в хостеле</figcaption>
  </figure>
  <br/>

  На завтрак можно было взять на кухне хлеб, мармелад, сыр, мюсли и хлопья, чай и кофе. Уютно очень. Единственный минус - хостел находится на холме и идти все время вверх 4 км утомительно. Машина бы решила этот вопрос. </p>
  <figure>
  <a href="img/IMG_9927.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_9927.jpg"/>
  </a>
  <figcaption>Братиславский град</figcaption>
  </figure>
  <br/>

<p>В конце января мы съездили на машине в <strong>Брно</strong>.
  <figure>
  <a href="img/IMG_0734.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_0734.jpg"/>
  </a>
  <figcaption>Брно</figcaption>
  </figure>
  <br/>
  Там было много снега и прекрасный замок Špilberk. Город мне понравился, но его мало видели - надо вернуться весной или осенью, когда будет хорошая погода. Потому что в январе было сыро и холодно. Но снежно. </p>

  <figure>
  <a href="img/IMG_0602.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_0602.jpg"/>
  </a>
  <figcaption>Брно</figcaption>
  </figure>
  <br/>
<p>А февраль был домашним. Много гуляли по Праге.</p>

<h2>Март и апрель</h2>
<p>Март был прекрасен городом <strong>Пльзень</strong>. Удивительно теплая погода для марта, гуляя в [зоопарке](https://lanadays.com/post/prague/2019-03-25-plzen-zoo/) раздевались до футболок - так солнце припекало. </p>
<figure>
<a href="img/IMG_4490.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_4490.jpg"/>
</a>
<figcaption>Зоопарк</figcaption>
</figure>
<br/>

<figure>
<a href="img/IMG_4637.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_4637.jpg"/>
</a>
<figcaption>Зоопарк</figcaption>
</figure>
<br/>
<p>На следующий день ездили в [Техноманию](https://lanadays.com/post/prague/2019-03-28-plzen-techmania/) - прекрасное место для детей и взрослых. Экперименты, опыты и много физики. Очень много места и все хочется попробовать и пощупать. </p>
<figure>
<a href="img/IMG_4875.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_4875.jpg"/>
</a>
<figcaption>Техмания</figcaption>
</figure>
<br/>
<p>  Лично меня поразил дом под углом, в нем кружится голова и начинает подташнивать. А еще поразила иммитация тонущего корабля: палуба под наклоном, гудит сирена, свет мигает, прям как в фильмах. Уххх, сколько эмоций. И город сам по себе интересный. Хочется вернуться. </p>
<figure>
<a href="img/IMG_4882.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_4882.jpg"/>
</a>
<figcaption>Косой дом в Техмании</figcaption>
</figure>
<br/>
<p>В апреле продолжили изучать города Чехии и побывали в [Кутной горе](https://lanadays.com/post/prague/2019-04-25-kutna-gora/), где находится знаменитая церковь из костей.</p>
<figure>
<a href="img/IMG_6920.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6920.jpg"/>
</a>
<figcaption>Кутна гора</figcaption>
</figure>
<br/>
<h2>Май и июнь</h2>
<p><strong>Брюссель</strong> мы посетили в мае.</p>
<figure>
<a href="img/IMG_0723.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_0723.jpg"/>
</a>
<figcaption>Европейский квартал</figcaption>
</figure>
<br/>
 <p>Неоднозначные воспоминания и впечатления. Прекрасные парки и европейский квартал, а через дорогу - поселение бомжей и свалка. </p>
 <figure>
 <a href="img/IMG_0893.jpg" target="_blank">
   <img width="600" height="450" border="0" align="center"  src="img/IMG_0893.jpg"/>
 </a>
 <figcaption>Дальше за деревьями поселение темнокожих бомжей</figcaption>
 </figure>
 <br/>

 <figure>
 <a href="img/IMG_0934.jpg" target="_blank">
   <img width="600" height="450" border="0" align="center"  src="img/IMG_0934.jpg"/>
 </a>
 <figcaption>Парк Лакен</figcaption>
 </figure>
 <br/>
   <p>И помимо всего удивительный Атомиум и зайцы в парке - как много можно успеть за выходные.</p>
 <figure>

 <a href="img/IMG_0772.jpg" target="_blank">
   <img width="600" height="450" border="0" align="center"  src="img/IMG_0772.jpg"/>
 </a>
 <figcaption>Парк Сэнкантёнэр и Триумфальная арка</figcaption>
 </figure>
 <br/>
 <figure>
 <a href="img/IMG_1077.jpg" target="_blank">
   <img width="600" height="450" border="0" align="center"  src="img/IMG_1077.jpg"/>
 </a>
 <figcaption>Атомиум</figcaption>
 </figure>
 <br/>
<p>В конце мая мы снова посетили <strong>Пльзень</strong> - ездили на [Metalfest open air](https://lanadays.com/post/prague/2019-06-03-metalfest-open-air/). У нас с дитем это был первый опыт такого масштабного феста. Было безумно жарко, мозги плавились. Мы к обеду приезжали на фест (жили в маленьком поселке в 30 км от феста) и валялись на траве в тени, слушая музыку. К вечеру уже ползли сцене поближе. Было просто офигенно!</p>
<figure>
<a href="img/IMG_1818.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_1818.jpg"/>
</a>
<figcaption>Metalfest Open Air</figcaption>
</figure>
<br/>
<p><strong>Фарерские острова</strong> в июне перечеркнули все ранее виденное. Это пока топ эмоций и впечатлений. Эта поездка была ни на что не похожа. Белые ночи, восхитительные виды, ночевки в палатке на берегу и комфортном двухэтажном доме. Когда мерзнет нос и постоянно идет дождь, а потом опп и раздеваешься до футболки, потому что вышло солнце и стало жарко. </p>
<figure>
<a href="img/IMG_2827.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_2827.jpg"/>
</a>
<figcaption>Ставим палатку на острове Воар</figcaption>
</figure>
<br/>

<figure>
<a href="img/IMG_3721.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_3721.jpg"/>
</a>
<figcaption>Взбираемся на холм от деревни Tjørnuvík</figcaption>
</figure>
<br/>

<figure>
<a href="img/IMG_3503.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_3503.jpg"/>
</a>
<figcaption>Остров Стреймой</figcaption>
</figure>
<br/>

<figure>
<a href="img/IMG_4073.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_4073.jpg"/>
</a>
<figcaption>Завтракаем в столице Tórshavn</figcaption>
</figure>
<br/>

<figure>
<a href="img/IMG_4363.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_4363.jpg"/>
</a>
<figcaption>Двухчасовой подъем на гору</figcaption>
</figure>
<br/>

<figure>
<a href="img/IMG_4420.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_4420.jpg"/>
</a>
<figcaption>Малыш-топтыш. Половина фоток - она ест</figcaption>
</figure>
<br/>

 <p>Ферментированное ужасно вонючее мясо и огромный выбор блюд из лосося на ужин в отеле. Да много чего было. Это было самое удивительное путешествие.</p>
 <p>Кто пропустил статьи про Фарерские острова:</p>

1. [Фарерские острова: подготовка](https://lanadays.com/post/travel/europe/faroe-islands/2019-06-27-faroe-islands-podgotovka/)

2. [Фарерские острова: день 1](https://lanadays.com/post/travel/europe/faroe-islands/2019-06-30-faroe-islands-day-1/)

3. [Фарерские острова: день 2](https://lanadays.com/post/travel/europe/faroe-islands/2019-07-02-faroe-islands-day-2/)

4. [Фарерские острова: день 3](https://lanadays.com/post/travel/europe/faroe-islands/2019-07-05-faroe-islands-day-3/)

5. [Фарерские острова: день 4](https://lanadays.com/post/travel/europe/faroe-islands/2019-07-11-faroe-islands-day-4/)

6. [Фарерские острова: день 5](https://lanadays.com/post/travel/europe/faroe-islands/2019-07-16-faroe-islands-day-5/)

6. [Фарерские острова: день 6-7](https://lanadays.com/post/travel/europe/faroe-islands/2019-07-20-faroe-islands-day-6-7/)

<h2>Июль-август</h2>
<p>В июле мы с дитем ездили на родину. Провели время с близкими людьми. Как же это было чудесно.</p>
<p>В августе сходили на концерт Metallica - это масса эмоций, такая энергетика! Незабываемо! </p>
<figure>
<a href="img/IMG_6342.JPG" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_6342.JPG"/>
</a>
<figcaption>Концерт Metallica</figcaption>
</figure>
<br/>
<p>На следующий день после концерта мы отправились в чудесное [путешествие](https://lanadays.com/post/travel/europe/italy/2019-09-02-na-avto-po-evrope/) по <strong>Хорватии и Италии</strong>.</p>
  <p>Как же много можно увидеть, если проехать на машине от Праги до Флоренции. По пути посетили удивительный [Загреб](https://lanadays.com/post/travel/europe/croatia/2019-09-09-zagreb/), неспешный Пунат и такой родной уже [Ровинь](https://lanadays.com/post/travel/europe/croatia/2019-09-13-ostrov-krk-i-rovinj/) (где забрали не менее родную подругу). Далее наконец-то доехали до Италии (никогда тут не была!). </p>
  <figure>
  <a href="img/IMG_6796.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_6796.jpg"/>
  </a>
  <figcaption>Пунат</figcaption>
  </figure>
  <br/>
  <figure>
  <a href="img/IMG_6938.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_6938.jpg"/>
  </a>
  <figcaption>Ровинь</figcaption>
  </figure>
  <br/>
  <figure>
  <a href="img/IMG_6941.JPG" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_6941.JPG"/>
  </a>
  <figcaption>Ровинь</figcaption>
  </figure>
  <br/>
  <p>  Начали с Вероны, перебрались в [Венецию](https://lanadays.com/post/travel/europe/italy/2019-09-16-avtomobilem-po-italii-verona-i-venice/). Впечатления от Венеции? Легко! Жарко и много туристов. Не распробовала, в общем. Надо вернуться весной или осенью. </p>
  <figure>
  <a href="img/IMG_666.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_666.jpg"/>
  </a>
  <figcaption>Венеция</figcaption>
  </figure>
  <br/>
  <figure>
  <a href="img/IMG_7056.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_7056.jpg"/>
  </a>
  <figcaption>Венеция</figcaption>
  </figure>
  <br/>
  <p>Далее Болонья и Флоренция. Красиво. </p>
  <figure>
  <a href="img/IMG_7653.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_7653.jpg"/>
  </a>
  <figcaption>Флоренция</figcaption>
  </figure>
  <br/>
  <figure>
  <a href="img/IMG_7692.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_7692.jpg"/>
  </a>
  <figcaption>Флоренция</figcaption>
  </figure>
  <br/>
  <p>Пиза впечатлила своей башней. Наконец-то я увидела Пизанскую башню вживую, а не на картинках. Потом пробегом по Генуи на обратном пути. Варацце отметили себе для обязательного посещения. Это безумно милый итальянский городок, жаль, что мы в нем только ночевали.
Так мы провели две недели отпуска: покупались в море, наелись морепродуктов, пасты и gelatto, напились лимончелло. Посетили места, которые видели на картинках. Незабываемый отдых.</p>
<figure>
<a href="img/IMG_7896.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7896.jpg"/>
</a>
<figcaption>Пизанская башня</figcaption>
</figure>
<br/>
<figure>
<a href="img/IMG_8004.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_8004.jpg"/>
</a>
<figcaption>Генуя</figcaption>
</figure>
<br/>
<h2>Сентябрь и октябрь</h2>
<p>В сентябре было затишье.  </p>
<p>В октябре мы ездили в <strong>Тшебонь</strong>. </p>
<figure>
<a href="img/IMG_8726.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_8726.jpg"/>
</a>
<figcaption>Тшебонь</figcaption>
</figure>
<br/>
  <p>Очень красивый городок. По нему можно часами гулять. Особенно по набережной. Но вечером все рестораны заняты - вот это проблема. </p>
  <figure>
  <a href="img/IMG_9074.jpg" target="_blank">
    <img width="600" height="450" border="0" align="center"  src="img/IMG_9074.jpg"/>
  </a>
  <figcaption>Тшебонь</figcaption>
  </figure>
  <br/>
<p>В конце месяца мы ездили в <strong>Ческе Будеёвице и Чески-Крумлов</strong>. С каждым новым посещенным городом я все больше убеждаюсь - я обожаю чешские города. Они удивительные и интересные каждый по своему. А еще я обожаю жить в отелях =) </p>
<figure>
<a href="img/IMG_9477.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_9477.jpg"/>
</a>
<figcaption>Ческе Будеёвице</figcaption>
</figure>
<br/>
<figure>
<a href="img/IMG_9548.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_9548.jpg"/>
</a>
<figcaption>Чески-Крумлов</figcaption>
</figure>
<br/>
<figure>
<a href="img/IMG_9652.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_9652.jpg"/>
</a>
<figcaption>Чески-Крумлов</figcaption>
</figure>
<br/>
<figure>
<a href="img/IMG_9706.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_9706.jpg"/>
</a>
<figcaption>Чески-Крумлов</figcaption>
</figure>
<br/>
<h2>Ноябрь и декабрь</h2>
<p>В ноябре мы летали на <strong>Мальту</strong>. Еще одно удивительное путешествие. </p>
<figure>
<a href="img/IMG_0485.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_0485.jpg"/>
</a>
<figcaption>Вид из окна в апартаментах на Мальте</figcaption>
</figure>
<br/>
<p>Я не могу [Мальту](https://lanadays.com/post/travel/europe/malta/2019-11-27-12-faktov-o-malte/) ни с чем сравнить. Вроде и Европа, а вроде и европейского в ней немного. Она особенная: своей природой, городами, едой и людьми. А еще мальтийский язык - особенное удовольствие.</p>
<figure>
<a href="img/IMG_0612.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_0612.jpg"/>
</a>
<figcaption>Мальта</figcaption>
</figure>
<br/>
<figure>
<a href="img/IMG_0810.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_0810.jpg"/>
</a>
<figcaption>Цитадель на острове Гоцо</figcaption>
</figure>
<br/>
<figure>
<a href="img/IMG_0875.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_0875.jpg"/>
</a>
<figcaption>Валетта</figcaption>
</figure>
<br/>
<figure>
<a href="img/IMG_1144.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_1144.jpg"/>
</a>
<figcaption>Мальта</figcaption>
</figure>
<br/>
<figure>
<a href="img/IMG_5181.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_5181.jpg"/>
</a>
<figcaption>Валетта</figcaption>
</figure>
<br/>
<p>Статьи по Мальте:</p>

1. [Мальта: впечатления и немного истории](https://lanadays.com/post/travel/europe/malta/2019-11-28-malta/)

2. [Что посмотреть в Валетте](https://lanadays.com/post/travel/europe/malta/2019-12-05-chto-posmotret-v-valette/)

3. [Мальта: остров Гоцо](https://lanadays.com/post/travel/europe/malta/2019-12-18-ostrov-gozo/)

<p>Завершили мы год (и начали следующий) Доломитовыми Альпами в Италии. Вот <strong>Южный Тироль</strong> совсем не итальянский. Здесь Италией и не пахнет. Мы жили в деревне, которая больше похожа на австрийскую. И много немецкого языка.</p>
<figure>
<a href="img/IMG_1803.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_1803.jpg"/>
</a>
<figcaption>Южный Тироль</figcaption>
</figure>
<br/>
<p>Катались на бордах и любовались горами. Это было прекрасно.</p>
<figure>
<a href="img/IMG_1976.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_1976.jpg"/>
</a>
<figcaption>Corvara in Badia</figcaption>
</figure>
<br/>
<figure>
<a href="img/IMG_1913.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_1913.jpg"/>
</a>
<figcaption>Апартменты Maierhof</figcaption>
</figure>
<br/>
<p>Это был насыщенный и счастливый год. </p>
