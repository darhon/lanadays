+++
banner     = "banners/2019-09-16-avtomobilem-po-italii-verona-i-venice.jpg"
title      = "Автомобилем по Италии: Верона и Венеция"
author     = "Lana"
date       = "2019-09-16"
categories = ["travel"]
tags       = ["europe", "italy", "fordfocus"]

+++

После недели проведенной в Хорватии, наш путь дальше лежал в солнечную Италию. Первым городом, куда мы устремились, была Верона. От Ровини до Вероны 370 км. Я переживала, что мы можем провести много времении на Хорватско-Словенской границе, все-таки мы снова въезжаем на территорию Шенгена. Поэтому в 7 утра мы уже выехали из Ровини. Но Гугл карты пророчили нам всего +6 минут к пути из-за небольшой тянучки на границе.

<!--more-->

Границу мы прошли очень быстро. На границе одно окошко хорватское, а метров через 10 окошко уже итальянское.

Часов в 11 мы остановились в Ristop - это место отдыха, большой паркинг, рестораны, магазин и заправка. Людей много. Мы решили не тратить время на заказ и ожидание еды в ресторане, поэтому за стойкой кафетерия выпили эспрессо, а в магазине купили прошутто, багет и питьевые йогурты. На парковке все это употребили.

## Верона

К обеду добрались до Вероны. Машину припарковали в 2 км от центра города.

<a href="img/IMG_7023.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7023.jpg"/>
</a>

В это время в Вероне проходит летний оперный фестиваль (в римском амфитеатре Арена ди Верона), а мы попали как раз на подготовку к нему. Вокруг Арены были разложены декорации внушительных размеров.

<a href="img/IMG_7006.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7006.jpg"/>
</a>

<a href="img/IMG_7013.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7013.jpg"/>
</a>

Сама Верона плотно асоциируется с Ромео и Джульеттой. И многие туристы как раз за этим и приезжают сюда.

В доме Джульетты главная достопримечательность конечно же балкон: чтобы туда подняться нужно выстоять длинную очередь.

<a href="img/IMG_7033.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7033.jpg"/>
</a>

Такая же очередь стоит к статуе Джульетты, что стоит во дворике. Есть примета, что потерев грудь Джульетты можно получить успех в любви. Вот и стремятся туристы потереть грудь Джульетты, а также оставить любовное послание на стене в арке при входе во дворик.

Сама Верона расположена на реке Адидже не привлекательного коричневого цвета. К сожалению, не удалось узнать, почему у воды такой цвет.

<a href="img/IMG_7004.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7004.jpg"/>
</a>

<a href="img/IMG_7025.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7025.jpg"/>
</a>

Прошлись немного улочками города и поехали в сторону Венеции в город Местре, в котором мы забронировали апартаменты в кемпинге.

## Кемпинг в Местре

Кемпинг классный. Есть места для караванов, небольшие домики с апартементами, в главном здании ресторан, рецепция и даже бассейн. Также есть кухня и детская площадка.
Отличное место, такое душевное.

Заселение после 16 часов. Мы как раз и приехали к этому времени. Кондиционер - 5 евро в день. Кроме этого нам предложили билеты на автобус до Венеции - 3 евро с человека в обе стороны.

Ресторан работает с 18 часов. Вернее кофе с круассанами выпить можно в течение дня, а вот поесть можно утром и вечером.
Кстати, предлагают завтраки: кофе, сок, круассаны и йогурты.

В километре от кемпинга есть супермаркет Lidl, где мы купили вина, закуски на вечер и йогурты с круассанами на завтрак.

Ужинали в ресторанчике в кемпинге пиццей. Большой выбор обалденно вкусной пиццы по 7-10 евро. Вариации пиццы всевозможные.

Апартаменты у нас были две комнаты и санузел. Места немного, тесновато, но спать весьма комфортно. Завтракали на веранде, а вечером здесь же пили вино с сыром и прошутто. Безжалостно поедаемые комарами.

## Венеция

Утром после душевного завтрака часов в 9 выдвинулись в Венецию. От кемпинга до автобусной остановки 5 минут пешком. На автобусе до Венеции еще 15 минут. Очень удобное размещение кемпинга.

<a href="img/IMG_7056.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7056.jpg"/>
</a>

Сразу скажу - на гондоле не катались. Удовольствие стоит 100 евро. Меня жаба задавила. Не жалею.
Чудесные узкие улочки, каналы, красивые домики и толпы туристов, сноющих по городу.

Очень много магазинчиков с масками и другой карнавальной аттрибутикой.

Что мы видели в Венеции:

<strong>Площадь Санкт-Марко и Собор Святого Марка</strong>

Сердце Венеции. Здесь находятся Дворец Дожей, Собор Святого Марка и кафе Флориан. В этом кафе были Гете, Казанова, Эрнест Хемингуэй и многие другие знаментые люди, которые сделали это место поистину популярным.

<a href="img/IMG_7173.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7173.jpg"/>
</a>

<a href="img/IMG_7174.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7174.jpg"/>
</a>

<strong>Смотровая площадка в ТРЦ (Fondaco Rooftop Terrace Venice)</strong>

Бесплатная (что удивительно для Венеции) смотровая площадка, которая находится в здании шопинг-мола. Чтобы не стоять в очереди, нужно забронировать визит. Выбрать дату и время посещения можно на [сайте](https://www.dfs.com/t-fondaco/rooftop-terrace-booking/booking/terrace_venice_en.html). Поднимаетесь на 4 этаж по эскалатору, который напоминает красную дорожку, показываете охраннику бронь и в положенное время поднимаетесь на крышу. У вас будет 15 минут насладиться видом и сделать фотографии.

<a href="img/IMG_7225.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7225.jpg"/>
</a>

<a href="img/IMG_7297.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7297.jpg"/>
</a>

<strong>Рыбный маркет и тусовка чаек</strong>

Проходя мимо рыбного рынка мы услышали неимоверный шум. Оказалось, что работники рынка выставляют коробки и пакеты с обрезками рыбы на улицу, где их растаскивают голодные чайки. Они устраивают настоящие скандалы и драки за рыбу, очень громко кричат.

<a href="img/IMG_7334.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7334.jpg"/>
</a>

<strong>Книжный магазин Libreria Acqua Alta</strong>

Это место нам показала моя подруга. Примечательно тем, что по центру магазина стоит гондола, в которой разложены книги. Тут есть оборудованное место для фотографий:

<a href="img/IMG_7361.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7361.jpg"/>
</a>

Книги новые и старые, мне кажется можно бесконечно их перебирать и рассматривать.

Кассир в магазине:

<a href="img/IMG_7396.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7396.jpg"/>
</a>

<strong>Палаццо Контарини дель Боволо</strong>

Чем примечателен этот Дворец? Ажурной винтовой лестницей и видом а Венецию, который открывается из-под купола Дворца.

<a href="img/IMG_7412.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7412.jpg"/>
</a>

<strong>Дворец Дожей</strong>

Самое посещаемое место в Венеции. Главное политическое здание в прежние времена: здесь заседал  Сенат и находился Верховный Суд. Дворец Дожей соединен с тюрьмой мостом, который прозвали "Мост вздохов". По этому мосту вели людей, которых ожидало заточение в тюрьме, отсюда и название.

<a href="img/IMG_7180.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7180.jpg"/>
</a>


<strong>Мост Риальто</strong>

Это первый и самый старый мост Венеции, который построили через Гранд-канал. Рядом с ним находится рынок Риальто, а на самом мосту сейчас располагаются сувенирные магазинчики.

Обедали мы в маленьком ресторанчике на одной из улочек подальше от центра города, где поменьше туристов. Ели равиоли с рикоттой и шпинатом под сырным соусом. Это было безумно вкусно. Ну и пили  Апероль Шприц, конечно же. Благо дело, мне за руль пока не нужно.

Целый день мы бродили по городу, прошли около 25 километров, ноги к вечеру гудели просто.
Удивительный город, не похожий ни на один город, который мы ранее видели. Но лично для меня это не самый очаровательный город в Италии. Да, красивый, да, уникальный, но в моем сердце он не отозвался.

<a href="img/IMG_7067.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7067.jpg"/>
</a>

Ужинали пиццей и салатами у себя в кемпинге.

<a href="img/IMG_7418.jpg" target="_blank">
  <img width="600" height="450" border="0" align="center"  src="img/IMG_7418.jpg"/>
</a>

Ребята еще сидели на веранде с лимончелло за беседой, а меня просто вырубило в 22 часа. Я даже не слышала, как они разошлись.

А утром мы едем дальше, понятное дело. Уже так привычно становится с утра собирать чемодан и запускать навигатор на следующий город.

Ну и видео, конечно же, по этим городам:
https://www.youtube.com/watch?v=1wCwL8FRXcI&t=578s
