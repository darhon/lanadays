+++
banner     = "banners/2019-02-20-produkty-v-prage.jpg"
title      = "Продукты в Праге"
author     = "Lana"
date       = "2019-02-20"
categories = ["prague"]
tags       = ["europe", "prague", "czechrepublic"]
+++
---
Я люблю поесть. Люблю интересные и необычные сочетания продуктов, пробовать новые блюда. Но все равно я остаюсь приверженцем домашней и здоровой пищи. Мне даже не жалко времени на покупку и приготовление еды для семьи. Благо дело выбор продуктов в Праге меня сильно порадовал, но есть конечно отличия в ассортименте пражских и киевских супермаркетов.

<!--more-->

![](img/2019-02-20-produkty-v-prage.jpg)

Прошло уже 2 месяца, как мы живем в Праге. Вскрывая последний пакет с украинской гречкой, чтобы приготовить ужин, подумалось о том, что мы привезли с собой 5 кг гречки. А из запасов остался всего один килограмм. Это ж надо было столько гречки съесть, мне казалось я ее готовлю редко. Но в маркетах я видела ее в продаже, так что я спокойна.
А еще оказалось, что выпили 2,25 кг кофе за два месяца. В основном выпила я. Муж кофе дома только по выходным пьет.
Вообще надо как-то заняться подсчетами потребления продуктов. Интересная статистика бы получилась.

## Что удивило в маркетах Праги

* В час пик работают все кассы, поэтому в магазине вроде людей и много, а вот на кассах очередь достаточно бодро двигается.
* Те посетители, которые не хотят брать тележку и не нашли свободных корзин - берут пустые коробки из-под продуктов возле кассы или приходят со своими.
Я на это все попервой смотрела большими глазами. Идешь такой с коробкой и собираешь в нее продукты с полок. А потом это кажется ужасно удобным.
* Торты и пирожные продаются далеко не везде. Захотелось тортика к чаю, обошли три маркета возле дома и нашли только тирамису.
Ребенку на День Рождения сделала тортик сама: купила заготовку, желе и консервированные абрикосы и клубнику.

![](img/cake_birthday.jpg)

* Существенные скидки на товары. Причем не такие, как в Киеве, когда заканчивается срок годности или повреждена упаковка, а на нормальные продукты. Например, перед новым годом в Альберте была акция на лосось, с 50% скидкой килограмм филе стоило 299 чешских крон.

![](img/ryba.jpg)

На масло недавно акция была, при обычной цене около 50 крон масло стоило 24 кроны. Причем срок годности - до 1 марта.

## Ассортимент продуктов

Молочка очень вкусная. Привычного нам в Киеве молока жирностью 2,5% - здесь нет. Теперь беру 1,5% для себя в кофе и 3,5% ребенку для хлопьев и на каши.

Творог в мягких пачках нашла недавно, немного не привычный нам, такой он тягучий немного, но сырники из него делала, нормально получается.

![](img/tvaroh_mekky.jpg)

Еще продается творог в пластиковых упаковках, по консистенции он как густая сметана.
Непривычно, что не пишут жирность крупным шрифтом, но как-то я и перестала заморачиваться с этим.

Кефир продается не везде. Я как любитель кефирчика на сон грядущий поначалу разыскивала его по всем маркетам, где была. Нашла только 1% kefirove mleko. Вкусно, но жидковато.
Но зато открыла для себя вполне годный заменитель - маложирные йогурты без сахара.
Йогуртов, кстати, тут всяких разных полно: с наполнителями и без них.

Большой выбор сыров и меня это очень радует. По ценам примерно как в Киеве, даже дешевле, но качество абсолютно другое. Была удивлена, как плавилась бюджетная гауда: мягкая и тянется, а не лежит резиновой лепешкой.
Местный Hermelin - с белой плесенью и грибным вкусом очень вкусный.

Мясо в упаковках и на развес. Интересно, что упаковки куриного филе, например, по 400, 600 и 900 грамм.
Перемолотое мясо упаковки по 500 грамм и по килограмму.

Популярны замороженные продукты: овощные и фруктовые смеси, рыбные палочки и наггетсы, пицца. В Киеве я никогда не покупала замороженную пиццу.
У нас возле дома была пиццерия, где готовили очень вкусную пиццу. Плюс бесплатная доставка делала это место действительно ценным.
А здесь я насмотрелась, как другие покупают пиццу из морозилки и решилась попробовать. Только не все пиццы одинаково хороши.
Я беру Dr. Oetker на тонком тесте. 15 минут в духовке и ребенок счастлив. Другие пробовали - не очень.
Удивлена замороженными эклерами, необычайно нежные и вкусные, не разлазятся после разморозки. Вот прям как свежие.

![](img/puffs.jpg)

Большой выбор фруктов и овощей. Экзотические фрукты более доступные по ценам, чем в Киеве. Из фруктов покупаем мандарины, апельсины, помело, свити, манго, яблоки.
Овощи в большинстве своем уже мытые, вот чтобы прям в земле, как у нас в Киеве иногда продаются - не видела.

## Цены на продукты
Вот тут самое интересное. Поначалу все цены в уме переводила в гривны. Но как-то быстро у меня это прошло, когда появилось понимание средних цен на продукты. Что-то дороже, чем в Киеве, но что-то и дешевле.
Продукты покупаю чаще всего по акции, они есть всегда и везде.

Что меня удивляет с непривычки в ценах, так это цены на картошку и яблоки. У нас они копеечные, а здесь нет.
Килограмм картошки около 20 крон. Продается на развес или в сетках по 1-2 кг. Чаще всего беру сетки по 2 кг за 40 крон.
Чистая и мытая.

Яблоки и бананы 18-20 крон.
Мандарины и апельсины по 20-30 крон (по акции бывают по 15).

Перемолотое мясо свинина-говядина около 100 крон за килограмм по акции.

![](img/meso.jpg)

Куриное филе по акции 90 крон за килограмм.
Куриная печень по 70 крон за килограмм.
Свинина по 100 крон.

Творог пачка или упаковка 250 грамм - 17-20 крон

![](img/tvaroh.jpg)

Йогурты стакан 500 мл - 12-18 крон.
Кефир 1 литр - до 20 крон.
Молоко 1 литр - 15-18 крон.
Яйца 30-40 крон (частенько бывают акции по 19 крон) за десяток.
Масло сливочное 40-50 крон пачка 250 грамм (частые акции по 20-30 крон)

Вот, к примеру, маркет Lidl недалеко от дома. Накупили с дитем продуктов на 450 чешских крон:

![](img/products.jpg)

Так что продукты здесь недорогие и качественные. Готовить одно удовольствие.
